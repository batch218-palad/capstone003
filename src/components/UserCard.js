//import {useState, useEffect} from 'react';
import { Button, Card } from 'react-bootstrap';
import PropTypes from 'prop-types';

import {Link} from 'react-router-dom';

export default function UserCard({user}) {

  const {firstName, lastName, email, mobileNo} = user;


  return (
  <Card>
      <Card.Body>
          <Card.Title>Last Name: {lastName}</Card.Title>
          <Card.Title>First Name: {firstName}</Card.Title>
          <Card.Subtitle>Email:</Card.Subtitle>
          <Card.Text>{email}</Card.Text>
          <Card.Subtitle>Mobile Number</Card.Subtitle>
          <Card.Text>{mobileNo}</Card.Text>
          <Button className="bg-secondary btn btn-secondary" as={Link} to={`/users/userDetails`} >Details</Button>
      </Card.Body>
  </Card>
  )
}

UserCard.propTypes = {
  user: PropTypes.shape({
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    mobileNo: PropTypes.string.isRequired
  })
}
