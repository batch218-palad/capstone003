/*import { useState, useEffect, useContext} from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';

import UserContext from '../UserContext';

import Swal from 'sweetalert2';

import {useParams, useNavigate, Link} from 'react-router-dom';

export default function UserView() {

	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	const {userId} = useParams();
	
	const [firstName, setFirstName] = useState(""); 
    const [lastName, setLastName] = useState(""); 
    const [email, setEmail] = useState("");
    const [mobileNo, setMobileNo] = useState("");

	const setAsAdmin = (userId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/updateUser`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isAdmin: true
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data){
				Swal.fire({
				  title: "Success!",
				  icon: "success",
				  text: "You have successfully set the user as admin."
				})

				navigate("/users");

				} else {
				  Swal.fire({
				  title: "Something went wrong / Account not authorized",
				  icon: "error",
				  text: "Please try again."
				})
			}
		})
	};


	useEffect(() => {

		

		fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`)

		.then(res => res.json())
		.then(data => {
			console.log(data);

			setFirstName(data.firstName);
			setLastName(data.lastName);
			setEmail(data.email);
			setMobileNo(data.mobileNo);
		})

	}, [userId])

	return (

		<Container>
			<Row>
				<Col lg={{span: 6, offset:3}} >
					<Card>
					      <Card.Body className="text-center">
					        <Card.Title>{firstName}{lastName}</Card.Title>
					        <Card.Subtitle>Email:</Card.Subtitle>
					        <Card.Text>{email}</Card.Text>
					        <Card.Subtitle>Mobile Number:</Card.Subtitle>
					        <Card.Text>{mobileNo}</Card.Text>
					       
					        
					        {
					        	(user.id !== null) ?
					        		<Button variant="secondary" onClick={() => setAsAdmin(userId)} >Set user as admin</Button>
					        		
					        		:
					        		<Button className="btn btn-danger" as={Link} to="/login"  >Log in to set as user as admin</Button>
					        }

					      </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

	)
}*/