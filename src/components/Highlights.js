import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights() {
	return (
	    <Row className="mt-3 mb-3">
	        <Col xs={12} md={4}>
	            <Card className="cardHighlight p-3">
	                <Card.Body>
	                    <Card.Title>
	                        <h2>Animated Decals</h2>
	                    </Card.Title>
	                    <Card.Text><img class="img-fluid  ml-lg-5 ml-md-4 mr-md-3" src="https://i.ebayimg.com/images/g/OSwAAOSwgENjSRXV/s-l1600.jpg"/>
	                        
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	        <Col xs={12} md={4}>
	            <Card className="cardHighlight p-3">
	                <Card.Body>
	                    <Card.Title>
	                        <h2>Skate Decals</h2>
	                    </Card.Title>
	                    <Card.Text><img class="img-fluid  ml-lg-5 ml-md-4 mr-md-3" src="https://i.ebayimg.com/images/g/2EcAAOSwhdBdEHqq/s-l1600.jpg"/>
	                        
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	        <Col xs={12} md={4}>
	            <Card className="cardHighlight p-3">
	                <Card.Body>
	                    <Card.Title>
	                        <h2>Car Decals</h2>
	                    </Card.Title>
	                    <Card.Text><img class="img-fluid  ml-lg-5 ml-md-4 mr-md-3" src="https://i.ebayimg.com/images/g/Qn8AAOSwTlJjSRLH/s-l1600.jpg"/>
	                        
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	    </Row>
	)
}