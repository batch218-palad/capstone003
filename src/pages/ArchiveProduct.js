//import { useContext } from 'react'; 
import { useParams, useNavigate} from 'react-router-dom'; 
import Swal from 'sweetalert2'; 

export default function Archive() {

    const { productId } = useParams();
    const navigate = useNavigate(); 

                fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
                    method: "PATCH",
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${localStorage.getItem('token')}`
                    },
                    body: JSON.stringify({
                    isActive: false
                })   
                    
                })
                .then(res => res.json()) 
                .then(data => {
                    console.log(data)

                    if(data) {
                 

                    Swal.fire({
                            title: "Item archived!",
                            icon: "success",
                            text: "Happy selling!"
                        })

                        navigate("/inventory");

                    } else {

                        Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "Please, try again."
      
                })
            }
        })
    }
    
