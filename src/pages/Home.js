import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home() {

	const data = {
		title: "Biz Shop",
		content: "Your one stop everyday shop.",
		destination: "/products",
		label: "View our decals!"

	}

	return (
		<>
		<Banner data={data} />
		<Highlights />
		</>
	)
}