import UserCard from '../components/UserCard';

import {useState, useEffect} from 'react';

export default function AllUser() {

	const [users, setUsers] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/allUser`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setUsers(data.map(user => {
				return (
					<UserCard key={user._id} user={user} />
				)
			}))
		})
	}, [])

	return(
		<>
		{users}
		</>
	)
}